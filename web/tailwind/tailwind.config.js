const { spacing } = require('tailwindcss/defaultTheme');
const colors = require('tailwindcss/colors');

const brandGreen = {
    '100': '#d6dace',
    '200': '#f1f4f0',
    '300': '#adc35a',
    '400': '#546a2a',
    '500': '#44571f',
    '600': '#44571f',
    '700': '#44571f',
    '800': '#44571f',
    '900': '#44571f'
}

const brandGrey = {
    '100': '#a0a0a0',
    '200': '#808285',
    '300': '#b9b9b9',
    '400': '#464745',
    '500': '#676767',
    '600': '#676767',
    '700': '#676767',
    '800': '#676767',
    '900': '#000000'
}

const brandPurple = {
    '100': '#3b2331',
    '200': '#3b2331',
    '300': '#3b2331',
    '400': '#3b2331',
    '500': '#3b2331',
    '600': '#3b2331',
    '700': '#3b2331',
    '800': '#3b2331',
    '900': '#3b2331'
}

module.exports = {
    theme: {
        extend: {
            screens: {
                'sm': '640px',
                // => @media (min-width: 640px) { ... }
                'md': '768px',
                // => @media (min-width: 768px) { ... }
                'lg': '1024px',
                // => @media (min-width: 1024px) { ... }
                'xl': '1220px',
                // => @media (min-width: 1280px) { ... }
                '2xl': '1536px',
                // => @media (min-width: 1536px) { ... }
            },
            colors: {
                primary: {
                    lighter: brandGreen['300'],
                    "DEFAULT": brandGreen['400'],
                    darker: brandGreen['900'],
                },
                secondary: {
                    lighter: colors.blue['100'],
                    "DEFAULT": colors.blue['200'],
                    darker: colors.blue['300'],
                },
                background: {
                    lighter: colors.blue['100'],
                    "DEFAULT": colors.blue['200'],
                    darker: colors.blue['300'],
                },
                green: brandGreen,
                grey: brandGrey,
                purple: brandPurple
            },
            textColor: {
                orange: colors.orange,
                primary: {
                    lighter: brandGrey['300'],
                    "DEFAULT": brandGrey['400'],
                    darker: brandGrey['900'],
                },
                secondary: {
                    lighter: colors.gray['400'],
                    "DEFAULT": colors.gray['600'],
                    darker: colors.gray['800'],
                },
            },
            backgroundColor: {
                primary: {
                    lighter: brandGreen['300'],
                    "DEFAULT": brandGreen['400'],
                    darker: brandGreen['500'],
                },
                secondary: {
                    lighter: colors.blue['100'],
                    "DEFAULT": colors.blue['200'],
                    darker: colors.blue['300'],
                },
                container: {
                    lighter: '#ffffff',
                    "DEFAULT": '#fafafa',
                    darker: '#f5f5f5',
                }
            },
            borderColor: {
                primary: {
                    lighter: brandGreen['300'],
                    "DEFAULT": brandGreen['400'],
                    darker: brandGreen['500'],
                },
                secondary: {
                    lighter: colors.blue['100'],
                    "DEFAULT": colors.blue['200'],
                    darker: colors.blue['300'],
                },
                container: {
                    lighter: '#f5f5f5',
                    "DEFAULT": '#e7e7e7',
                    darker: '#b6b6b6',
                }
            },
            minWidth: {
                8: spacing["8"],
                20: spacing["20"],
                40: spacing["40"],
                48: spacing["48"],
            },
            minHeight: {
                14: spacing["14"],
                'screen-25': '25vh',
                'screen-50': '50vh',
                'screen-75': '75vh',
            },
            maxHeight: {
                '0': '0',
                'screen-25': '25vh',
                'screen-50': '50vh',
                'screen-75': '75vh',
            }
        },
    },
    variants: {
        extend: {
            borderWidth: ['last', 'hover', 'focus'],
            margin: ['last'],
            opacity: ['disabled'],
            backgroundColor: ['even', 'odd'],
            ringWidth: ['active']
        }
    },
    plugins: [
        require('@tailwindcss/forms'),
        require('@tailwindcss/typography'),
    ],
    purge: {
        // Examples for excluding patterns from purgeX
        // options: {
        //     safelist: [/^bg-opacity-/, /^-?[mp][trblxy]?-[4,8]$/, /^text-shadow/],
        // },
        content: [
            // this theme's phtml files
            '../../**/*.phtml',
            // parent theme in Vendor (if this is a child-theme)
            //'../../../../../../../vendor/hyva-themes/magento2-default-theme/**/*.phtml',
            // app/code phtml files (if need tailwind classes from app/code modules)
            //'../../../../../../../app/code/**/*.phtml',
            // react app src files (if Hyvä Checkout is installed in app/code)
            //'../../../../../../../app/code/**/src/**/*.jsx',
            // react app src files in vendor (If Hyvä Checkout is installed in vendor)
            //'../../../../../../../vendor/hyva-themes/magento2-hyva-checkout/src/reactapp/src/**/*.jsx',
            // widget block classes from app/code
            //'../../../../../../../app/code/**/Block/Widget/**/*.php'
        ]
    }
}
